#![no_std]
extern crate alloc;

use alloc::boxed::Box;
use core::marker::PhantomData;

pub mod fields;
pub mod ops;

pub use fields::{constant, function, identity, wrap};

pub trait ScalarField {
	type Point;
	type Scalar;
	fn value(&self, point: Self::Point) -> Self::Scalar;

	fn map<S, F>(self, func: F) -> MapField<Self, F>
	where
		Self: Sized,
		F: Fn(Self::Scalar) -> S,
	{
		MapField { inner: self, func }
	}

	fn transform<P, F>(self, func: F) -> TransformField<Self, P, F>
	where
		Self: Sized,
		F: Fn(P) -> Self::Point,
	{
		TransformField {
			inner: self,
			func,
			_point: PhantomData,
		}
	}
}

#[derive(Copy, Clone, Debug)]
pub struct MapField<I, F> {
	inner: I,
	func: F,
}

impl<P, S> ScalarField for Box<dyn ScalarField<Point = P, Scalar = S>> {
	type Point = P;
	type Scalar = S;

	fn value(&self, point: Self::Point) -> Self::Scalar {
		(**self).value(point)
	}
}

impl<I, S, F> ScalarField for MapField<I, F>
where
	I: ScalarField,
	F: Fn(I::Scalar) -> S,
{
	type Point = I::Point;
	type Scalar = S;

	fn value(&self, point: Self::Point) -> Self::Scalar {
		(self.func)(self.inner.value(point))
	}
}

#[derive(Copy, Clone, Debug)]
pub struct TransformField<I, P, F> {
	inner: I,
	func: F,
	_point: PhantomData<P>,
}

impl<I, P, F> ScalarField for TransformField<I, P, F>
where
	I: ScalarField,
	F: Fn(P) -> I::Point,
{
	type Point = P;
	type Scalar = I::Scalar;

	fn value(&self, point: Self::Point) -> Self::Scalar {
		self.inner.value((self.func)(point))
	}
}

#[cfg(test)]
mod tests {
	use crate::*;

	#[test]
	fn test_constant() {
		assert_eq!(
			((constant(1) + constant(5) * constant(3)) / constant(4)).value(()),
			4,
		);
	}

	#[test]
	fn test_function() {
		let field = function(|(x, y, z)| x + y - z);
		assert_eq!(field.value((4, 5, 6)), 3);
		assert_eq!(field.value((10, 11, 12)), 9);
		assert_eq!(field.value((-2, -3, 4)), -9);
	}

	#[test]
	fn test_identity() {
		assert_eq!(identity().value(1), 1);
		assert_eq!(identity().value(-2), -2);
		assert_eq!(identity().value([1, 2, 3]), [1, 2, 3]);
	}

	#[test]
	fn test_map() {
		assert_eq!(constant(3).map(|x| x * 2).value(()), 6);
		assert_eq!(identity().map(|(x, y)| x + y).value((-4, 11)), 7);
	}

	#[test]
	fn test_transform() {
		assert_eq!(constant(3).transform(|p| p * 2).value(4), 3);
		assert_eq!(
			identity()
				.map(|(x, y)| (x * 2.0, y / 2.0))
				.value((3.0, 4.0)),
			(6.0, 2.0)
		);
	}
}
