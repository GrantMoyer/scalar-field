extern crate alloc;

use crate::{
	fields::{ConstantField, FunctionField, IdentityField, ScalarFieldWrapper},
	MapField, ScalarField, TransformField,
};
use alloc::boxed::Box;
use core::ops::{Add, BitAnd, BitOr, BitXor, Div, Mul, Neg, Not, Rem, Shl, Shr, Sub};

macro_rules! binary_ops {
	{$dollar:tt $($name:ident $func:ident $wrapper:ident $macro:ident;)*} => {
		$(
			#[derive(Copy, Clone, Debug)]
			pub struct $wrapper<L, R> {
			    lhs: L,
			    rhs: R,
			}

			impl<L, R> ScalarField for $wrapper<L, R>
			where
			    L: ScalarField,
			    R: ScalarField<Point = L::Point>,
			    L::Scalar: $name<R::Scalar>,
			    L::Point: Clone,
			{
			    type Point = L::Point;
			    type Scalar = <L::Scalar as $name<R::Scalar>>::Output;

			    fn value(&self, point: Self::Point) -> Self::Scalar {
			        self.lhs.value(point.clone()).$func(self.rhs.value(point))
			    }
			}

			macro_rules! $macro {
				{impl <$dollar($g:tt),*> Op for $t:ty where $dollar($bounds:tt)*} => {
					impl<Rhs $dollar(,$g)* > $name<Rhs> for $t
					where
					    Rhs: ScalarField<Point = <$t as ScalarField>::Point>,
					    <$t as ScalarField>::Scalar: $name<Rhs::Scalar>,
					    $dollar($bounds)*
					{
					    type Output = $wrapper<$t, Rhs>;

					    fn $func(self, rhs: Rhs) -> Self::Output {
					        Self::Output { lhs: self, rhs }
					    }
					}
				}
			}
		)*
	}
}

macro_rules! unary_ops {
	{$dollar:tt $($name:ident $func:ident $wrapper:ident $macro:ident;)*} => {
		$(
			#[derive(Copy, Clone, Debug)]
			pub struct $wrapper<T> {
			    inner: T,
			}

			impl<T> ScalarField for $wrapper<T>
			where
			    T: ScalarField,
			    T::Scalar: $name,
			{
			    type Point = T::Point;
			    type Scalar = <T::Scalar as $name>::Output;

			    fn value(&self, point: Self::Point) -> Self::Scalar {
			        self.inner.value(point).$func()
			    }
			}

			macro_rules! $macro {
				{impl <$dollar($g:tt),*> Op for $t:ty where $dollar($bounds:tt)*} => {
					impl<$dollar($g),*> $name for $t
					where
					    <$t as ScalarField>::Scalar: $name,
					    $dollar($bounds)*
					{
					    type Output = $wrapper<$t>;

					    fn $func(self) -> Self::Output {
					        Self::Output { inner: self }
					    }
					}
				}
			}
		)*
	}
}

binary_ops! {$
	Add add AddField impl_add;
	BitAnd bitand BitAndField impl_bitand;
	BitOr bitor BitOrField impl_bitor;
	BitXor bitxor BitXorField impl_bitxor;
	Div div DivField impl_div;
	Mul mul MulField impl_mul;
	Rem rem RemField impl_rem;
	Shl shl ShlField impl_shl;
	Shr shr ShrField impl_shr;
	Sub sub SubField impl_sub;
}

unary_ops! {$
	Neg neg NegField impl_neg;
	Not not NotField impl_not;
}

macro_rules! impl_ops {
	{$($token:tt)*} => {
		impl_add!{$($token)*}
		impl_bitand!{$($token)*}
		impl_bitor!{$($token)*}
		impl_bitxor!{$($token)*}
		impl_div!{$($token)*}
		impl_mul!{$($token)*}
		impl_rem!{$($token)*}
		impl_shl!{$($token)*}
		impl_shr!{$($token)*}
		impl_sub!{$($token)*}

		impl_neg!{$($token)*}
		impl_not!{$($token)*}
	}
}

impl_ops! {
	impl<L, R> Op for AddField<L, R>
	where
		L: ScalarField,
		R: ScalarField<Point = L::Point>,
		L::Scalar: Add<R::Scalar>,
		L::Point: Clone,
}
impl_ops! {
	impl<L, R> Op for SubField<L, R>
	where
		L: ScalarField,
		R: ScalarField<Point = L::Point>,
		L::Scalar: Sub<R::Scalar>,
		L::Point: Clone,
}
impl_ops! {
	impl<L, R> Op for MulField<L, R>
	where
		L: ScalarField,
		R: ScalarField<Point = L::Point>,
		L::Scalar: Mul<R::Scalar>,
		L::Point: Clone,
}
impl_ops! {
	impl<L, R> Op for DivField<L, R>
	where
		L: ScalarField,
		R: ScalarField<Point = L::Point>,
		L::Scalar: Div<R::Scalar>,
		L::Point: Clone,
}
impl_ops! {
	impl<T> Op for NegField<T>
	where
		T: ScalarField,
		T::Scalar: Neg,
}

impl_ops! {
	impl<P, S> Op for Box<dyn ScalarField<Point = P, Scalar = S>>
	where
}
impl_ops! {
	impl<I, S, F> Op for MapField<I, F>
	where
		I: ScalarField,
		F: Fn(I::Scalar) -> S,
}
impl_ops! {
	impl<I, P, F> Op for TransformField<I, P, F>
	where
		I: ScalarField,
		F: Fn(P) -> I::Point,
}

impl_ops! {
	impl<Lhs> Op for ScalarFieldWrapper<Lhs>
	where
		Lhs: ScalarField,
}
impl_ops! {
	impl<P, S> Op for ConstantField<P, S>
	where
		S: Clone,
}
impl_ops! {
	impl<P, S, F> Op for FunctionField<P, F>
	where
		F: Fn(P) -> S,
}
impl_ops! {
	impl<T> Op for IdentityField<T>
	where
}
