use crate::ScalarField;
use core::marker::PhantomData;

#[derive(Copy, Clone, Debug)]
pub struct ScalarFieldWrapper<T>(T);

impl<T: ScalarField> From<T> for ScalarFieldWrapper<T> {
	fn from(inner: T) -> Self {
		Self(inner)
	}
}

pub fn wrap<T: ScalarField>(inner: T) -> ScalarFieldWrapper<T> {
	ScalarFieldWrapper::from(inner)
}

impl<T: ScalarField> ScalarField for ScalarFieldWrapper<T> {
	type Point = T::Point;
	type Scalar = T::Scalar;

	fn value(&self, point: Self::Point) -> Self::Scalar {
		self.0.value(point)
	}
}

#[derive(Copy, Clone, Debug)]
pub struct ConstantField<P, S>(S, PhantomData<P>);

impl<P, S: Clone> ScalarField for ConstantField<P, S> {
	type Point = P;
	type Scalar = S;

	fn value(&self, _: Self::Point) -> Self::Scalar {
		self.0.clone()
	}
}

pub fn constant<P, S: Clone>(inner: S) -> ConstantField<P, S> {
	ConstantField(inner, PhantomData)
}

#[derive(Copy, Clone, Debug)]
pub struct FunctionField<P, F>(F, PhantomData<P>);

impl<P, S, F> ScalarField for FunctionField<P, F>
where
	F: Fn(P) -> S,
{
	type Point = P;
	type Scalar = S;

	fn value(&self, point: Self::Point) -> Self::Scalar {
		self.0(point)
	}
}

pub fn function<P, S, F: Fn(P) -> S>(func: F) -> FunctionField<P, F> {
	FunctionField(func, PhantomData)
}

#[derive(Copy, Clone, Debug)]
pub struct IdentityField<T>(PhantomData<T>);

impl<T> ScalarField for IdentityField<T> {
	type Point = T;
	type Scalar = T;

	fn value(&self, point: Self::Point) -> Self::Scalar {
		point
	}
}

pub fn identity<T>() -> IdentityField<T> {
	IdentityField(PhantomData)
}
